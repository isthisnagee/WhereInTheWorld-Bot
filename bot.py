import praw, time, os, requests
from requests import HTTPError        # to escape ban errors
import sys                            # for all errors
import re
import textwrap                       # for multiline comments
import json
reddit = praw.Reddit('WhereInTheWorld by /u/ManU_Fan10ne')
reddit.login()
#FIX Keyerror[0] issue.
call = ['whereintheworld', 'witw', 'where in the world']
while True:
    answered = []
    for comment in reddit.get_comments('test'):
        has_match = any(word in str(comment).lower() for word in call)
        if has_match and comment.id not in answered:
            answered.append(comment.id)
            words = str(comment).split()
            r = requests.get('http://restcountries.eu/rest/v1/name/{}'.format(words[-1]))
            text_r = r.text
            country = json.loads(text_r)
            c = country[0]

            r_image = requests.get('https://duckduckgo.com/?q=where+is+{}&iax=1&ia=images&format=json'.format(words[-1]))
            image = json.loads(r_image.text)

            reply = textwrap.dedent("""
                    Hello! You said "Where in the world!"


                    Here's some info about {}.


                    {} is [here]({})


                    Population: {}


                    Main Language: {}

                    """.format(c['name'], c['name'], image['Image'], c['population'],
                                c['languages'][0]))
            try:
                comment.reply(reply)
            # If you are banned from a subreddit, reddit throws a 403 instead of a helpful message :/
            except HTTPError as err:
                print("Probably banned from /r/" + str(comment.subreddit), file=sys.stderr)
            # This one is pretty rare, since PRAW controls the rate automatically, but just in case
            except KeyError as err:
                print("Rate Limit Exceeded:\n" + str(err), file=sys.stderr)
                time.sleep(err.sleep_time)
